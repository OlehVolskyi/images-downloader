package com.volskyioleh.imagedownloader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class StorageSaverTask extends AsyncTask<Bitmap, Void, String> {
    private SaveCallback mSaveCallback;
    private int mIndex;

    StorageSaverTask(Context context, int index) {
        this.mSaveCallback = ((StorageSaverTask.SaveCallback) context);
        mIndex = index;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mSaveCallback.isSave(s, mIndex);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Bitmap... bitmaps) {
        Bitmap bitmap = bitmaps[0];
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "MyApplication");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File pictureFile = new File(directory, imageFileName);
        try {
            pictureFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream out = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            return pictureFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface SaveCallback {
        void isSave(String patch, int index);
    }
}
