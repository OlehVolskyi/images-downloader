package com.volskyioleh.imagedownloader.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class ItemModel {

    public ItemModel(String mUrl, String mPatch, Long mDate) {
        this.mUrl = mUrl;
        this.mPatch = mPatch;
        this.mDate = mDate;
    }

    @ColumnInfo(name = "url")
    private String mUrl;

    @ColumnInfo(name = "patch")
    private String mPatch;

    @ColumnInfo(name = "date")
    private Long mDate;

    public String getPatch() {
        return mPatch;
    }

    public void setPatch(String mPatch) {
        this.mPatch = mPatch;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }



    public Long getDate() {
        return mDate;
    }

    public void setDate(Long mDate) {
        this.mDate = mDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
