package com.volskyioleh.imagedownloader.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

public class AddNewImageViewModel extends AndroidViewModel {

    private ItemsDB mItemsDB;

    public AddNewImageViewModel(@NonNull Application application) {
        super(application);

        mItemsDB = ItemsDB.getDatabase(getApplication());
    }

    public void addItem(final ItemModel newItem) {
        new  AddNewImageTask(mItemsDB).execute(newItem);
    }

    private class AddNewImageTask extends AsyncTask<ItemModel, Void, Void> {

        private ItemsDB db;

        AddNewImageTask(ItemsDB mItemsDB) {
            db = mItemsDB;
        }

        @Override
        protected Void doInBackground(ItemModel... itemModels) {
            db.itemModelDao().insertAll(itemModels[0]);
            return null;
        }
    }


}
