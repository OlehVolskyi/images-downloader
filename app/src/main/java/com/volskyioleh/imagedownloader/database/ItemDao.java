package com.volskyioleh.imagedownloader.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ItemDao {

    @Query("SELECT id,patch,date,url FROM itemmodel WHERE patch IS NOT NULL ORDER BY date DESC")
    LiveData<List<ItemModel>> getAllItems();

    @Query("SELECT id,patch,date,url FROM itemmodel WHERE patch IS NULL ORDER BY date")
    List<ItemModel> getNotDownloadItems();


    @Query("UPDATE itemmodel SET patch =:patch WHERE id = :id")
    void update(String patch, int id);


    @Insert(onConflict = REPLACE)
    void insertAll(ItemModel itemModels);
}
