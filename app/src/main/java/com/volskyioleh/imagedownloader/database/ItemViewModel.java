package com.volskyioleh.imagedownloader.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {

    private final LiveData<List<ItemModel>> mList;

    public ItemViewModel(@NonNull Application application) {
        super(application);
        ItemsDB mAppDatabase = ItemsDB.getDatabase(this.getApplication());
        mList = mAppDatabase.itemModelDao().getAllItems();
    }

    public LiveData<List<ItemModel>> getItemsList() {
        return mList;
    }
}
