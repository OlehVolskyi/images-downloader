package com.volskyioleh.imagedownloader.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = ItemModel.class, version = 1, exportSchema = false)
public abstract class ItemsDB extends RoomDatabase {

    private static ItemsDB mInstance;

    public static ItemsDB getDatabase(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(), ItemsDB.class, "items_db")
                    .build();
        }
        return mInstance;
    }

    public static void destroyInstances() {
        mInstance = null;
    }

    public abstract ItemDao itemModelDao();

}
