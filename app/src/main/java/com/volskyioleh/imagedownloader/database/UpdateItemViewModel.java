package com.volskyioleh.imagedownloader.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;

public class UpdateItemViewModel extends AndroidViewModel {

    private ItemsDB mItemsDB;

    public UpdateItemViewModel(@NonNull Application application) {
        super(application);

        mItemsDB = ItemsDB.getDatabase(getApplication());
    }

    public void updateItem(final List<ItemModel> newItem) {
        new UpdateItemsTask(mItemsDB).execute(newItem);
    }


    private class UpdateItemsTask extends AsyncTask<List<ItemModel>, Void, Void> {

        private ItemsDB db;

        UpdateItemsTask(ItemsDB mItemsDB) {
            db = mItemsDB;
        }

        @Override
        protected Void doInBackground(List<ItemModel>... lists) {
            for (int i = 0; i < lists[0].size(); i++) {
                db.itemModelDao().update(lists[0].get(i).getPatch(),lists[0].get(i).getId());
            }
            return null;
        }
    }


}
