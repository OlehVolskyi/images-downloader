package com.volskyioleh.imagedownloader.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class NotDownloadedItemsViewModel extends AndroidViewModel {


    private final ItemsDB mAppDatabase;

    public NotDownloadedItemsViewModel(@NonNull Application application) {
        super(application);
       mAppDatabase = ItemsDB.getDatabase(this.getApplication());

    }

    public List<ItemModel> getItemsList() {
        try {
            return new GetNotDowloadedItemsTask(mAppDatabase).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class GetNotDowloadedItemsTask extends AsyncTask<Void, Void, List<ItemModel>> {

        private ItemsDB db;

        GetNotDowloadedItemsTask(ItemsDB mItemsDB) {
            db = mItemsDB;
        }


        @Override
        protected List<ItemModel> doInBackground(Void... voids) {
            return db.itemModelDao().getNotDownloadItems();
        }
    }
}
