package com.volskyioleh.imagedownloader;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.volskyioleh.imagedownloader.database.AddNewImageViewModel;
import com.volskyioleh.imagedownloader.database.ItemModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddImageActivity extends AppCompatActivity implements ImageDownloader.TaskCallback, StorageSaverTask.SaveCallback {

    private static final String APP_PREFERENCES = "IS_OFFLINE_ADD";
    private static final String OFFLINE_KEY = "OFFLINE_KEY";
    @BindView(R.id.edit_text)
    EditText mEditTextForUrl;
    @BindView(R.id.previewImage)
    ImageView mPreviewImage;
    @BindView(R.id.fab_add_new_item)
    FloatingActionButton mFloatingActionButton;


    private boolean mSaveKey = false;
    private Bitmap mBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.fab_add_new_item)
    public void fabClick() {
        if (mEditTextForUrl.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Fill in the field or add an image", Toast.LENGTH_SHORT).show();
        } else {
            if (isNetworkAvailable() && mBitmap == null) {
                mSaveKey = true;
                new ImageDownloader(this, 0).execute(stringToURL(String.valueOf(mEditTextForUrl.getText())));
            } else if (isNetworkAvailable() && mBitmap != null) {
                new StorageSaverTask(this, 0).execute(mBitmap);
            } else {
                SharedPreferences preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                preferences.getBoolean(OFFLINE_KEY, false);
                SharedPreferences.Editor ed = preferences.edit();
                ed.putBoolean(OFFLINE_KEY, true);
                ed.apply();
                new StorageSaverTask(this, 0).execute(mBitmap);
            }
        }
    }

    @OnClick(R.id.button_check)
    public void checkClick() {
        new ImageDownloader(this, 0).execute(stringToURL(String.valueOf(mEditTextForUrl.getText())));
    }

    protected URL stringToURL(String urlString) {
        try {
            return new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void isDownloaded(Bitmap bitmap, int index) {
        if (bitmap != null) {
            mPreviewImage.setImageBitmap(bitmap);
            mBitmap = bitmap;
            if (mSaveKey) {
                new StorageSaverTask(this, 0).execute(bitmap);
                mSaveKey = false;
            }
        } else {
            Snackbar.make(mFloatingActionButton, "Your URL is not valid", Snackbar.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void isSave(String patch, int index) {
        new AddNewImageViewModel(getApplication()).addItem(new ItemModel(String.valueOf(mEditTextForUrl.getText()), patch, new Date().getTime()));
            finish();
    }

}
