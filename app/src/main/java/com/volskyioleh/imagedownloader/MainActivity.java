package com.volskyioleh.imagedownloader;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.volskyioleh.imagedownloader.database.ItemModel;
import com.volskyioleh.imagedownloader.database.ItemViewModel;
import com.volskyioleh.imagedownloader.database.NotDownloadedItemsViewModel;
import com.volskyioleh.imagedownloader.database.UpdateItemViewModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements NetworkChangeReceiver.BroadcastCallback,
        ImageDownloader.TaskCallback, StorageSaverTask.SaveCallback {

    private ItemsAdapter mAdapter;
    private List<ItemModel> mList;
    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;

    private static final String APP_PREFERENCES = "IS_OFFLINE_ADD";
    private static final String OFFLINE_KEY = "OFFLINE_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAdapter = new ItemsAdapter(new ArrayList<ItemModel>(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        ItemViewModel mItemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        mItemViewModel.getItemsList().observe(MainActivity.this, new Observer<List<ItemModel>>() {
            @Override
            public void onChanged(@Nullable List<ItemModel> itemModels) {
                mAdapter.addItem(itemModels);
            }
        });
        registerReceiver(
                new NetworkChangeReceiver(),
                new IntentFilter(
                        ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStoragePermissionGranted();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {

            case 1232: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    System.exit(0);
                }
            }
        }
    }

    public void isStoragePermissionGranted() {
        String TAG = "PERMISSIONS";
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1232);
            }
        } else {
            Log.v(TAG, "Permission is granted");
        }
    }

    @OnClick(R.id.fab)
    public void fabClick() {
        startActivity(new Intent(MainActivity.this, AddImageActivity.class));
    }

    @Override
    public void idConnected(Boolean isConnected) {
        if (isConnected) {
            SharedPreferences preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            if (preferences.getBoolean(OFFLINE_KEY, false)) {
                mList = new NotDownloadedItemsViewModel(getApplication()).getItemsList();
                if (mList != null) {
                    for (int i = 0; i < mList.size(); i++) {
                        new ImageDownloader(this, i).execute(stringToURL(mList.get(i).getUrl()));
                    }
                }
                SharedPreferences.Editor ed = preferences.edit();
                ed.putBoolean(OFFLINE_KEY, false);
                ed.apply();
            }
        }
    }

    protected URL stringToURL(String urlString) {
        try {
            return new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void isDownloaded(Bitmap bitmap, int index) {
        if (bitmap != null) {
            new StorageSaverTask(this, index).execute(bitmap);
        }
    }

    @Override
    public void isSave(String patch, int index) {
        if (patch != null) {
            mList.get(index).setPatch(patch);
            new UpdateItemViewModel(getApplication()).updateItem(mList);
        }
    }
}
