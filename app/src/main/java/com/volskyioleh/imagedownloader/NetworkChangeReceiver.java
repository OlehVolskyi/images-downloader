package com.volskyioleh.imagedownloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {


    private boolean isConnected = false;
    private NetworkChangeReceiver.BroadcastCallback mBroadcastCallback;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.mBroadcastCallback = ((NetworkChangeReceiver.BroadcastCallback) context);
        isNetworkAvailable(context);
    }

    private void isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        if (!isConnected) {
                            isConnected = true;
                           mBroadcastCallback.idConnected(true);
                        }
                        return;
                    }
                }
            }
        mBroadcastCallback.idConnected(false);
        Toast.makeText(context, "Internet connection is not available, you are in offline mode", Toast.LENGTH_LONG).show();
        isConnected = false;
    }

    public interface BroadcastCallback{
        void idConnected(Boolean isConnected);
    }
}
