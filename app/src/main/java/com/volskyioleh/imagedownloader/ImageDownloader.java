package com.volskyioleh.imagedownloader;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageDownloader extends AsyncTask<URL, Void, Bitmap> {

    private Context mContext;
    private ProgressDialog mProgressDialog;
    private TaskCallback mTaskCallback;
    private int mIndex;

    ImageDownloader(Context context, int index) {
        mContext = context;
        this.mTaskCallback = ((TaskCallback) context);
        createProgressDialod();
        mIndex  = index;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog.show();
    }

    private void createProgressDialod() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setTitle("Downloading...");
        mProgressDialog.setMessage("Please wait, we are downloading your image file...");
    }


    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        mProgressDialog.dismiss();
        mTaskCallback.isDownloaded(result, mIndex);
    }

    @Override
    protected Bitmap doInBackground(URL... urls) {
        URL url = urls[0];
        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            return BitmapFactory.decodeStream(bufferedInputStream);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }


   public interface TaskCallback{
      void isDownloaded(Bitmap bitmap, int index);
    }
}
