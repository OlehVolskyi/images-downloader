package com.volskyioleh.imagedownloader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.volskyioleh.imagedownloader.database.ItemModel;

import java.io.File;
import java.util.List;


class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemViewHolder> {
    private List<ItemModel> mList;
    private Context mContext;

    ItemsAdapter(List<ItemModel> itemsList, Context context) {
        mList = itemsList;
        mContext = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ItemModel model = mList.get(position);
        Glide.with(mContext).load(new File(model.getPatch())).into(holder.mItemImage);
    }

    public void addItem(List<ItemModel> itemModels) {
        mList = itemModels;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        // @BindView(R.id.item_image)
        ImageView mItemImage;

        ItemViewHolder(View itemView) {
            super(itemView);
            mItemImage = itemView.findViewById(R.id.item_image);
            //     ButterKnife.bind(this, itemView);
        }
    }
}
